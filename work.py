# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields, ModelView, Workflow
from trytond.pyson import Eval


class Work(Workflow, metaclass=PoolMeta):
    __name__ = 'production.work'
    employee = fields.Many2One('company.employee', 'Employee',
        required=False)
    datetime_effective = fields.Function(fields.DateTime('DT Effective'),
        'get_datetime_effective')
    unit_price = fields.Numeric('Unit Price', digits=(16, 2), depends=['operation'])

    @classmethod
    def __setup__(cls):
        super(Work, cls).__setup__()
        cls.state_string = super(Work, cls).state.translated('state')
        cls.state.selection.append(('paid', 'Paid'))
        cls._transitions |= set((
                ('draft', 'running'),
                ('running', 'finished'),
                ('finished', 'paid'),
            ))
        cls._buttons.update({
            'running': {
                'invisible': Eval('state') != 'draft',
                },
            'finished': {
                'invisible': Eval('state') != 'running',
                },
            'paid': {
                'invisible': Eval('state') != 'finished',
                },
            })

    @classmethod
    @ModelView.button
    @Workflow.transition('running')
    def running(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('paid')
    def paid(cls, records):
        pass

    def get_datetime_effective(self, name=None):
        if self.write_date:
            return self.write_date

    @fields.depends('operation', 'unit_price')
    def on_change_operation(self):
        if not self.operation:
            return
        self.unit_price = self.operation.unit_price
