# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import production
from . import work
from . import payroll
from . import bom


def register():
    Pool.register(
        work.Work,
        bom.BOM,
        production.Production,
        configuration.Configuration,
        production.ProductionFixNumberStart,
        production.ScheduleByTaskStart,
        production.Operation,
        production.ProductionPayoffStart,
        bom.BOMService,
        module='production_task', type_='model')
    Pool.register(
        production.ScheduleByTaskReport,
        production.TaskReport,
        production.ProductionPayoffReport,
        module='production_task', type_='report')
    Pool.register(
        production.ProductionFixNumber,
        production.ScheduleByTask,
        payroll.PayrollPayoffProduction,
        production.AddOperations,
        production.ProductionPayoff,
        module='production_task', type_='wizard')
