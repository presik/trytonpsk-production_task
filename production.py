# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from sql import Table
from trytond.model import ModelView, Workflow, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

STATES_DONE = {'readonly': Eval('state') == 'done'}


class Production(metaclass=PoolMeta):
    __name__ = 'production'
    party = fields.Many2One('party.party', 'Party',
        states=STATES_DONE)
    attribute_set = fields.Many2One('product.attribute.set', 'Set')
    attributes = fields.Dict('product.attribute', 'Attributes',
        domain=[
            ('sets', '=', Eval('attribute_set')),
            ],
        states={
            'readonly': ~Eval('attribute_set'),
            },
        depends=['attribute_set'])
    date_delivery = fields.DateTime('Date of Delivery',
        states=STATES_DONE)
    workforce_cost = fields.Function(fields.Numeric('Workforce Cost'),
        'get_workforce_cost')
    total_cost = fields.Function(fields.Numeric('Total Cost'),
        'get_total_cost')
    customer_comment = fields.Text('Comment', states=STATES_DONE)
    color = fields.Char('Color', states=STATES_DONE)
    plantilla = fields.Char('Plantilla', states=STATES_DONE)
    banda = fields.Char('Banda', states=STATES_DONE)
    planta = fields.Char('Planta', states=STATES_DONE)

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls.state_string = super(Production, cls).state.translated('state')

    def get_workforce_cost(self, name=None):
        res = Decimal(0)
        return res

    def get_total_cost(self, name=None):
        res = self.workforce_cost + self.cost
        return res

    @classmethod
    def wait(cls, productions):
        super(Production, cls).wait(productions)
        for p in productions:
            services = {s.operation.id: s.cost_price for s in p.bom.services}
            for work in p.works:
                if services.get(work.operation.id):
                    work.unit_price = services[work.operation.id]
                    work.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, productions):
        # pool = Pool()
        # Work = pool.get('production.work')
        # super(Production, cls).cancel(productions)
        pass


class Operation(metaclass=PoolMeta):
    __name__ = 'production.routing.operation'
    unit_price = fields.Numeric('Unit Price', digits=(16, 2))


class TaskReport(Report):
    __name__ = 'production.task'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        new_records = []

        for rec in records:
            dkeys = sorted(rec.attributes.keys())
            for i in range(8):
                value = ''
                attrib = ''
                if i < len(dkeys):
                    attrib = dkeys[i]
                    value = rec.attributes[attrib]
                setattr(rec, 'att' + str(i + 1), attrib)
                setattr(rec, 'val' + str(i + 1), value)
            new_records.append(rec)
        report_context['records'] = new_records
        return report_context


class ProductionFixNumberStart(ModelView):
    "Production Fix Number Start"
    __name__ = 'production_task.fix_number.start'
    number = fields.Char('New Number', required=True)


class ProductionFixNumber(Wizard):
    "Production Fix Number"
    __name__ = 'production_task.fix_number'
    start = StateView('production_task.fix_number.start',
        'production_task.production_fix_number_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        production = Table('production')
        id_ = Transaction().context['active_id']
        cursor = Transaction().connection.cursor()
        if id_:
            cursor.execute(*production.update(
                columns=[production.number],
                values=[self.start.number],
                where=production.id == id_),
            )
        return 'end'


class ScheduleByTaskStart(ModelView):
    "Production Schedule By Task Start"
    __name__ = 'production_task.schedule_by_task.start'
    start_date = fields.Date('Start Date', required=True)
    routing = fields.Many2One('production.routing', 'Routing', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    attribute_set = fields.Many2One('product.attribute.set', 'Attribute',
        required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_routing():
        Routing = Pool().get('production.routing')
        routings = Routing.search([])
        if routings:
            return routings[0].id


class ScheduleByTask(Wizard):
    "Schedule By Task"
    __name__ = 'production_task.schedule_by_task'
    start = StateView('production_task.schedule_by_task.start',
        'production_task.schedule_by_task_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('production_task.report_schedule_by_task')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'routing': self.start.routing.id,
            'attribute_set': self.start.attribute_set.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ScheduleByTaskReport(Report):
    __name__ = 'production_task.report_schedule_by_task'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Routing = pool.get('production.routing')
        AttibuteSet = pool.get('product.attribute.set')
        Production = pool.get('production')
        Company = pool.get('company.company')
        MAX_LIMIT = 10
        dom_pcc = [
            ('planned_date', '>=', data['start_date']),
            ('routing', '=', data['routing']),
            ('state', '!=', ['cancel']),
            ('attribute_set', '=', data['attribute_set']),
        ]
        productions = Production.search(dom_pcc)
        routing = Routing(data['routing'])

        attributes_set = AttibuteSet(data['attribute_set'])

        operations = {'operation' + str(i + 1): '' for i in range(MAX_LIMIT)}
        attributes = {'attribute' + str(i + 1): '' for i in range(MAX_LIMIT)}
        report_context.update({'total_attribute' + str(i + 1): 0 for i in range(MAX_LIMIT)})
        report_context.update(attributes)
        dict_attributes = {}

        i = 0
        for at in attributes_set.attributes:
            i += 1
            attribute_name = 'attribute' + str(i)
            attributes[attribute_name] = at
            report_context[attribute_name] = at.name
            dict_attributes[at.name] = attribute_name

        seq = 0
        for step in routing.steps:
            seq += 1
            op_num = 'operation' + str(seq)
            report_context[op_num] = step.operation.name[:4]
            if operations.get(step.sequence):
                operations[step.sequence] = op_num

        for i in range(MAX_LIMIT - seq):
            seq += 1
            op_num = 'operation' + str(seq)
            report_context[op_num] = ''

        records = []
        for pcc in productions:
            for k, v in operations.items():
                setattr(pcc, k, v)
            for k, v in attributes.items():
                setattr(pcc, k, '')

            if pcc.attributes:
                for attr, value in pcc.attributes.items():
                    if dict_attributes.get(attr):
                        setattr(pcc, dict_attributes[attr], value)
                        report_context['total_' + dict_attributes[attr]] += value or 0

            count_works = 0

            if not pcc.works:
                continue

            for w in pcc.works:
                if w.state in ['draft', 'request', 'waiting']:
                    continue

                setattr(pcc, 'operation' + str(w.sequence), w.state_string)
                count_works += 1
            progress = 0
            if count_works != 0:
                progress = (float(count_works) / len(pcc.works)) * 100
            pcc.progress = progress
            records.append(pcc)

        report_context['records'] = records
        report_context['routing'] = routing
        report_context['start_date'] = data['start_date']
        report_context['company'] = Company(data['company'])

        return report_context


class AddOperations(Wizard):
    "Add Operations"
    __name__ = 'production_task.add_operations'
    start_state = 'add_operations'
    add_operations = StateTransition()

    def transition_add_operations(self):
        pool = Pool()
        Production = pool.get('production')
        Work = pool.get('production.work')
        ids = Transaction().context['active_ids']
        productions = Production.browse(ids)
        to_create = []
        for pcc in productions:
            if not pcc.routing:
                continue

            count = 0
            for step in pcc.routing.steps:
                count += 1
                to_create.append({
                    'production': pcc.id,
                    'sequence': count,
                    'operation': step.operation.id,
                    'work_center': pcc.work_center.id,
                    'company': pcc.company.id,
                    'state': 'request',
                })

        Work.create(to_create)
        return 'end'


class ProductionPayoffStart(ModelView):
    "Production Payoff Start"
    __name__ = 'production_task.payoff.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ProductionPayoff(Wizard):
    "Production Payoff"
    __name__ = 'production_task.payoff'
    start = StateView('production_task.payoff.start',
        'production_task.production_payoff_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('production_task.report_payoff')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ProductionPayoffReport(Report):
    __name__ = 'production_task.report_payoff'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Production = pool.get('production')
        Company = pool.get('company.company')
        productions = Production.search([
            ('planned_date', '>=', data['start_date']),
            ('planned_date', '<=', data['end_date']),
        ])

        employees = {}
        for pcc in productions:
            for work in pcc.works:
                if work.state != 'finished':
                    continue
                if work.employee.id not in employees:
                    employees[work.employee.id] = {
                        'name': work.employee.party.name,
                        'amount': 0,
                    }
                employees[work.employee.id]['amount'] = work.unit_price * Decimal(work.production.quantity)

        report_context['records'] = employees.values()
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['company'] = Company(data['company'])
        return report_context
