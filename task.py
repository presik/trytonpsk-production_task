# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, If
from trytond.report import Report
from trytond.transaction import Transaction

from .exceptions import TaskLineStateError

STATES = {'readonly': (Eval('state') != 'draft')}

STATES_LINE = {'readonly': (Eval('state') != 'realization')}

_ZERO = Decimal('0.0')


class Task(Workflow, ModelSQL, ModelView):
    "Production Task"
    __name__ = 'production.task'
    _rec_name = 'number'
    number = fields.Char("Number", readonly=True)
    company = fields.Many2One('company.company', 'Company', required=True,
        states={
            'readonly': (Eval('state') != 'draft') | Eval('lines', [0]),
            },
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        depends=['state'])
    task_date = fields.Date('Task Date', required=True, states=STATES,
            )
    product = fields.Many2One('product.product', 'Product',
            required=True, states=STATES, domain=[
                ('type', '=', 'goods'),
            ])
    party = fields.Many2One('party.party', 'Party', required=True,
        states={
            'readonly': ((Eval('state') != 'draft') | (Eval('lines', [0]) & Eval('party'))),
            },
        depends=['state'])
    reference = fields.Many2One('product.price_list', 'Price List',
        domain=[('company', '=', Eval('company'))], required=True,
        states={
            'readonly': (Eval('state') != 'draft'),
            }, depends=['state', 'company'])
    lines = fields.One2Many('production.task.line', 'task', 'Lines',
            context={
                'quantity': Eval('quantity'),
                'lines': Eval('lines'),
            }, states={
                'readonly': Eval('state') == 'done',
                'required': Eval('state') == 'done',
            })
    description = fields.Char("Description", states=STATES)
    quantity = fields.Integer("Quantity", states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('realization', 'Realization'),
            ('done', 'Done'),
            ], 'State', readonly=True)
    unit = fields.Many2One('product.uom', 'Unit', depends=['product'],
            states={'readonly': True})
    total_cost = fields.Function(fields.Numeric("Total Cost",
            depends=['lines', 'reference']), 'get_total_cost')
    comment = fields.Text("Comment", states=STATES)
    attributes = fields.One2Many('production.task.attribute', 'task', 'Attributes',
            context={
                'product': Eval('product'),
            }, states={
                'readonly': Eval('state') == 'done',
                'required': Eval('state') == 'done',
            })

    @classmethod
    def __setup__(cls):
        super(Task, cls).__setup__()
        cls._order.insert(0, ('task_date', 'DESC'))
        cls._transitions |= set((
                ('draft', 'realization'),
                ('realization', 'draft'),
                ('realization', 'done'),
                ('done', 'draft'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'done': {
                    'invisible': Eval('state') != 'realization',
                    },
                'realization': {
                    'invisible': Eval('state') != 'draft',
                    },
                'add_attributes': {
                    'invisible': Bool(Eval('lines')),
                    },
                'create_lines': {
                    'invisible': Bool(Eval('lines')),
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_task_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for rec in records:
            for line in rec.lines:
                if line.state != 'done':
                    raise TaskLineStateError(
                        gettext('production_task.msg_lines_not_done'))

    @classmethod
    @ModelView.button
    @Workflow.transition('realization')
    def realization(cls, records):
        for rec in records:
            rec.set_number()

    @classmethod
    @ModelView.button
    def add_attributes(cls, records):
        for rec in records:
            rec._add_attributes()

    @classmethod
    @ModelView.button
    def create_lines(cls, records):
        TaskLine = Pool().get('production.task.line')
        lines_to_create = []

        for record in records:
            for line in record.reference.lines:
                lines_to_create.append({
                    'sequence': line.sequence,
                    'task': record.id,
                    'work_service': line.product.id,
                    'quantity': record.quantity,
                    'unit_price': _ZERO,
                    'state': 'realization',
                })

        new_lines = TaskLine.create(lines_to_create)
        for nl in new_lines:
            nl.on_change_work_service()
            nl.save()

    def get_total_cost(self, name=None):
        res = _ZERO
        for line in self.lines:
            if line.cost_amount:
                res += line.cost_amount
        return res

    @fields.depends('unit', 'product')
    def on_change_with_unit(self, name=None):
        if self.product:
            return self.product.default_uom.id

    def set_number(self):
        """
        Fill the number field with the sale sequence
        """
        pool = Pool()
        Config = pool.get('production.configuration')

        config = Config(1)
        if self.number:
            return
        number = config.task_sequence.get()
        self.write([self], {'number': number})

    def _add_attributes(self):
        if not self.product:
            return
        Attribute = Pool().get('production.task.attribute')
        attribute_to_create = []
        for attr in self.product.attributes:
            attribute_to_create.append({
                'task': self.id,
                'attribute': attr,
            })
        Attribute.create(attribute_to_create)


class TaskLine(Workflow, ModelSQL, ModelView):
    "Production Task Line"
    __name__ = 'production.task.line'
    task = fields.Many2One('production.task', 'Task', required=True)
    sequence = fields.Integer('Sequence', required=False)
    employee = fields.Many2One('company.employee', 'Employee',
            states={
                'required': Eval('state') == 'done',
            }, depends=['state'])
    task_date = fields.Date('Task Date', states={
                'required': Eval('state') == 'done',
            })
    work_service = fields.Many2One('product.product', 'Work Service',
            domain=[
                ('type', '=', 'service'),
                ('purchasable', '=', True),
            ], required=True)
    unit_price = fields.Numeric("Unit Price", digits=(16, 2),
            depends=['quantity', 'work_service'], required=True)
    quantity = fields.Integer("Quantity", required=True)
    cost_amount = fields.Numeric("Cost Amount", states={
            'readonly': True,
            }, depends=['quantity', 'work_service'])
    state = fields.Selection([
            ('realization', 'Realization'),
            ('done', 'Done'),
            ('paid', 'Paid'),
            ], 'State', readonly=True)
    notes = fields.Text("Notes")

    @classmethod
    def __setup__(cls):
        super(TaskLine, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))
        cls._transitions |= set((
                ('realization', 'done'),
                ('done', 'realization'),
                ('done', 'paid'),
        ))
        cls._buttons.update({
                'done': {
                    'invisible': Eval('state') != 'realization',
                    },
                'realization': {
                    'invisible': Eval('state') != 'done',
                    },
                'paid': {
                    'invisible': Eval('state') != 'done',
                    },
                })

    @staticmethod
    def default_quantity():
        if Transaction().context.get('quantity'):
            return Transaction().context.get('quantity')

    @staticmethod
    def default_cost_amount():
        return _ZERO

    @staticmethod
    def default_state():
        return 'realization'

    @classmethod
    @ModelView.button
    @Workflow.transition('realization')
    def realization(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('paid')
    def paid(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        today = Pool().get('ir.date').today()
        for rec in records:
            if not rec.task_date:
                rec.write([rec], {'task_date': today})

    @fields.depends('quantity', 'work_service', 'task', 'cost_amount',
    '_parent_task.reference', 'unit_price', '_parent_task.company')
    def on_change_work_service(self):
        if self.quantity and self.work_service and self.task and \
            self.task.reference:

            quantity = self.quantity or 0
            unit_price = self.task.reference.compute(
                self.task.company,
                self.work_service,
                self.work_service.template.cost_price,
                quantity,
                self.work_service.template.default_uom,
            )
            if unit_price:
                self.unit_price = unit_price.quantize(
                    Decimal(1) / 10 ** self.__class__.unit_price.digits[1])
            else:
                self.unit_price = _ZERO
            self.cost_amount = unit_price * self.quantity
        else:
            self.cost_amount = _ZERO

    @fields.depends('work_service', 'quantity', 'task', 'cost_amount',
        '_parent_task.reference', 'unit_price')
    def on_change_quantity(self):
        if not self.work_service or not self.unit_price or not self.quantity:
            return
        self.cost_amount = self.unit_price * self.quantity

    @fields.depends('work_service', 'quantity', 'cost_amount', 'unit_price')
    def on_change_unit_price(self):
        if not self.quantity and not self.unit_price:
            return
        self.cost_amount = self.unit_price * self.quantity


class TaskReport(Report):
    __name__ = 'production.task'


class TaskAttribute(ModelSQL, ModelView):
    "Production Task Attribute"
    __name__ = 'production.task.attribute'
    task = fields.Many2One('production.task', 'Task', required=True)
    attribute = fields.Char('Attribute', required=True)
    value = fields.Char('Value')
