# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'
    services = fields.One2Many('production.bom.service', 'bom', 'Services')


class BOMService(ModelSQL, ModelView):
    "Bill of Material Services"
    __name__ = 'production.bom.service'
    bom = fields.Many2One('production.bom', 'BOM', required=True, ondelete='CASCADE')
    operation = fields.Many2One('production.routing.operation',
        'Operation', required=True)
    cost_price = fields.Numeric('Cost Price', required=True)

    @classmethod
    def __setup__(cls):
        super(BOMService, cls).__setup__()
